/**
Copyright (C) 2013, 2021-2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use anyhow::Result;
use mwapi_responses::{prelude::*, protection::ProtectionInfo};
use mwbot::timestamp::Expiry;
use mwbot::Bot;
use serde::Deserialize;
use serde_json::Value;
use std::collections::HashMap;
use time::format_description::FormatItem;
use time::macros::format_description;
use time::{Date, Duration, OffsetDateTime};
use tracing::info;

const YY_MM_DD: &[FormatItem] = format_description!("[year]-[month]-[day]");
const MW_TIMESTAMP: &[FormatItem] =
    format_description!("[year]-[month]-[day]T[hour]:[minute]:[second]Z");

/// Format of "Template:TFA title/data.json"
#[derive(Deserialize)]
struct TfaData {
    titles: HashMap<String, Titles>,
}

#[derive(Deserialize)]
#[serde(untagged)]
enum Titles {
    Single(String),
    Multiple(Vec<String>),
}

impl Titles {
    fn to_vec(&self) -> Vec<String> {
        match self {
            Titles::Single(title) => vec![title.to_string()],
            Titles::Multiple(titles) => titles.to_owned(),
        }
    }
}

#[query(prop = "info", inprop = "protection")]
struct ProtectionResponse;

/// Query the current protection status of a page
async fn protection_status(
    name: &str,
    bot: &Bot,
) -> Result<Vec<ProtectionInfo>> {
    let resp: ProtectionResponse =
        mwapi_responses::query_api(bot.api(), &[("titles", name)]).await?;
    Ok(resp.query.pages[0].protection.clone())
}

async fn handle_page(
    name: &str,
    day: Date,
    bot: &Bot,
    // Whether we also need to apply edit protection
    redirect: bool,
) -> Result<()> {
    let status = protection_status(name, bot).await?;
    // dbg!(&status);
    // let title = Title::new_from_full(name, api);
    // Protect for a day
    let until = day + Duration::days(1);
    let mut move_protected = false;
    let mut edit_protected = false;
    for prot in status.iter() {
        if prot.level == "sysop" {
            if prot.type_ == "move" {
                if prot.expiry.is_infinity() {
                    move_protected = true;
                } else {
                    let expiry = prot
                        .expiry
                        .as_timestamp()
                        .unwrap()
                        .format("%Y-%m-%d")
                        .to_string();
                    if time::Date::parse(
                        &expiry,
                        format_description!("[year]-[month]-[day]"),
                    )? >= until
                    {
                        move_protected = true;
                    }
                }
            } else if prot.type_ == "edit" {
                if prot.expiry.is_infinity() {
                    edit_protected = true;
                } else {
                    let expiry = prot
                        .expiry
                        .as_timestamp()
                        .unwrap()
                        .format("%Y-%m-%d")
                        .to_string();
                    if time::Date::parse(
                        &expiry,
                        format_description!("[year]-[month]-[day]"),
                    )? >= until
                    {
                        edit_protected = true;
                    }
                }
            }
        }
    }
    if move_protected && (!redirect || edit_protected) {
        info!("{} is already protected", name);
        return Ok(());
    }
    info!("{} needs to be protected!", name);

    let mut protections = vec![];
    let mut expiry = vec![];
    let mut cascade = false;
    if !move_protected {
        protections.push("move=sysop".to_string());
        expiry.push(
            until
                .with_hms(0, 0, 0)
                // unwrap: Safe because we hardcode 0s
                .unwrap()
                .format(&MW_TIMESTAMP)
                .unwrap(),
        )
    }
    if redirect && !edit_protected {
        protections.push("edit=sysop".to_string());
        expiry.push(
            until
                .with_hms(0, 0, 0)
                // unwrap: Safe because we hardcode 0s
                .unwrap()
                .format(&MW_TIMESTAMP)
                .unwrap(),
        )
    }
    for prot in status.iter() {
        if prot.type_ == "aft" {
            // bug 57389
            continue;
        }
        if prot.source.is_some() {
            // skip cascading protection
            continue;
        }
        if (prot.type_ == "move" && !move_protected)
            || (prot.type_ == "edit" && redirect && !edit_protected)
        {
            // don't try to protect what we're changing
            continue;
        }
        if prot.cascade {
            // send it back i guess?
            cascade = true;
        }
        protections.push(format!("{}={}", prot.type_, prot.level));
        // TODO: upstream to mwapi_responses
        let formatted = match prot.expiry {
            Expiry::Infinity => "infinity".to_string(),
            Expiry::Finite(ts) => ts.to_string(),
        };
        expiry.push(formatted);
    }

    let protections = protections.join("|");
    let expiry = expiry.join("|");
    let mut params = vec![
        ("action", "protect"),
        ("title", name),
        ("protections", &protections),
        ("expiry", &expiry),
        ("reason", "Upcoming TFA ([[WP:BOT|bot protection]])"),
    ];
    if cascade {
        params.push(("cascade", "1"));
    }
    let _: Value = bot.api().post_with_token("csrf", &params).await?;
    info!("Successfully protected {}", &name);
    Ok(())
}

async fn load_tfa_data(bot: &Bot) -> Result<TfaData> {
    Ok(serde_json::from_str(
        &bot.page("Template:TFA title/data.json")?.wikitext().await?,
    )?)
}

#[tokio::main]
async fn main() -> Result<()> {
    std::env::set_var("RUST_LOG", "mwapi=debug,info");
    tracing_subscriber::fmt::init();
    let now = OffsetDateTime::now_utc().date();
    let bot = Bot::from_default_config().await?;
    let tfa_data = load_tfa_data(&bot).await?;
    for ahead in 1..35 {
        let day = now + Duration::days(ahead);
        let formatted_day = day.format(YY_MM_DD).unwrap();
        if let Some(titles) = tfa_data.titles.get(&formatted_day) {
            for text in titles.to_vec() {
                let page = bot.page(&text)?;
                match page.redirect_target().await? {
                    Some(target) => {
                        info!("{} redirects to {}", &text, &target.title());
                        handle_page(&text, day, &bot, true).await?;
                        handle_page(target.title(), day, &bot, false).await?;
                    }
                    None => {
                        handle_page(&text, day, &bot, false).await?;
                    }
                }
            }
        } else {
            info!("{} is missing, skipping", day.format(YY_MM_DD).unwrap());
            continue;
        }
    }
    info!("Finished successfully!");
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::Path;
    use time::{
        macros::{date, time},
        PrimitiveDateTime,
    };

    async fn test_bot() -> Bot {
        Bot::from_path(Path::new("mwbot-test.toml")).await.unwrap()
    }

    #[tokio::test]
    async fn test_protection_status() {
        let bot = test_bot().await;
        assert!(protection_status("User:Legoktm/test", &bot)
            .await
            .unwrap()
            .is_empty());
        let einstein =
            protection_status("Albert Einstein", &bot).await.unwrap();
        assert_eq!(einstein[0].type_, "edit".to_string());
        assert_eq!(einstein[0].level, "autoconfirmed".to_string());
        assert!(einstein[0].expiry.is_infinity());
        assert_eq!(einstein[1].type_, "move".to_string());
        assert_eq!(einstein[1].level, "sysop".to_string());
        assert!(einstein[1].expiry.is_infinity());
    }

    #[test]
    fn test_mw_timestamp() {
        let ts = "2001-01-15T14:56:00Z";
        let dt = PrimitiveDateTime::parse(ts, &MW_TIMESTAMP)
            .unwrap()
            .assume_utc();
        assert_eq!(dt.date(), date!(2001 - 01 - 15));
        assert_eq!(dt.time(), time!(14:56:00));
        let formatted = dt.format(&MW_TIMESTAMP).unwrap();
        assert_eq!(&formatted, ts);
    }

    #[tokio::test]
    async fn test_extract_tfa_title() {
        let bot = test_bot().await;
        let tfa_data = load_tfa_data(&bot).await.unwrap();
        assert_eq!(
            vec!["Zoo TV Tour".to_string()],
            tfa_data.titles.get("2020-02-29").unwrap().to_vec(),
        );
        // Case normalization
        assert_eq!(
            vec!["Mosaics of Delos".to_string()],
            tfa_data.titles.get("2020-02-05").unwrap().to_vec(),
        );
        // Unicode (was broken in Python)
        assert_eq!(
            vec!["SMS Zähringen".to_string()],
            tfa_data.titles.get("2020-02-06").unwrap().to_vec(),
        );
        // Italics
        assert_eq!(
            vec!["The Cabinet of Dr. Caligari".to_string()],
            tfa_data.titles.get("2020-02-26").unwrap().to_vec(),
        );
    }
}
