use anyhow::Result;
use clap::Parser;
use mwbot::{
    generators::{self, Generator},
    Bot, SaveOptions,
};
use serde::Deserialize;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    page: Vec<String>,

    #[arg(short, long)]
    ns: Vec<String>,

    #[arg(long, default_value_t)]
    exclude_redirect: bool,

    #[arg(short, long, default_value_t)]
    edit_refresh: bool,

    #[arg(
        short,
        long,
        default_value = "Bot: Purge page cache by saving the current revision again"
    )]
    summary: String,
}

#[derive(Deserialize)]
struct Response {
    purge: Vec<PurgeResponse>,
}

#[derive(Deserialize)]
struct PurgeResponse {
    // ns: i32,
    // title: String,
    purged: bool,
}

// @TODO: replace after next mwbot release
#[derive(Generator)]
#[params(generator = "transcludedin", glhlimit = "max")]
pub struct TranscludedIn {
    #[param("titles")]
    titles: Vec<String>,
    #[param("gtinamespace")]
    namespaces: Option<Vec<u32>>,
    #[param("gtishow")]
    filter: Option<generators::link::Filter>,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();
    let bot = Bot::from_default_config().await?;

    let mut purged = 0;

    let mut linkshere = TranscludedIn::new(args.page);
    if !args.ns.is_empty() {
        linkshere = linkshere.namespaces(
            args.ns
                .iter()
                .map(|n| {
                    // @TODO: replace with Bot.namespace_id
                    bot.title_codec()
                        .namespace_map()
                        .get_id(n.as_str())
                        .unwrap_or_else(|| panic!("NS {} not found", n))
                        as u32
                })
                .collect::<Vec<_>>(),
        );
    }
    if args.exclude_redirect {
        linkshere = linkshere.filter(generators::link::Filter::NonRedirect);
    }
    let mut linkshere = linkshere.generate(&bot);

    while let Some(page) = linkshere.recv().await {
        let mut page = page?;

        println!("* {}", page.title());

        if args.edit_refresh {
            page = {
                let wt = page.wikitext().await?;
                let (new_page, resp) =
                    page.save(wt, &SaveOptions::summary(&args.summary)).await?;
                assert!(resp.nochange);
                new_page
            };
        }

        let resp: Response = bot
            .api()
            .post_with_token(
                "csrf",
                &[("action", "purge"), ("titles", page.title())],
            )
            .await?;
        if resp.purge[0].purged {
            purged += 1;
        } else {
            println!("failed to purge: {}", page.title());
        }
    }

    println!("==== Purged {} pages", purged);

    Ok(())
}
