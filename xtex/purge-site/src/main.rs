use std::time::Duration;

use anyhow::Result;
use clap::Parser;
use mwbot::{
    generators::{self, AllPages, FilterRedirect, Generator},
    Bot, SaveOptions,
};
use serde::Deserialize;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    ns: String,

    #[arg(long, default_value_t)]
    exclude_redirect: bool,

    #[arg(short, long, default_value_t)]
    edit_refresh: bool,

    #[arg(
        short,
        long,
        default_value = "Bot: Purge page cache by saving the current revision again"
    )]
    summary: String,

    #[arg(short, long, default_value = "5000")]
    delay: u64,
}

#[derive(Deserialize)]
struct Response {
    purge: Vec<PurgeResponse>,
}

#[derive(Deserialize)]
struct PurgeResponse {
    // ns: i32,
    // title: String,
    #[serde(default)]
    purged: bool,
}

// @TODO: replace after next mwbot release
#[derive(Generator)]
#[params(generator = "transcludedin", glhlimit = "max")]
pub struct TranscludedIn {
    #[param("titles")]
    titles: Vec<String>,
    #[param("gtinamespace")]
    namespaces: Option<Vec<u32>>,
    #[param("gtishow")]
    filter: Option<generators::link::Filter>,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();
    let bot = Bot::from_default_config().await?;

    let mut purged = 0;

    let mut allpages = AllPages::new().namespace(
        // @TODO: replace with Bot.namespace_id
        bot.title_codec()
            .namespace_map()
            .get_id(args.ns.as_str())
            .unwrap_or_else(|| panic!("NS {} not found", &args.ns))
            as u32,
    );
    if args.exclude_redirect {
        allpages = allpages.filter_redirect(FilterRedirect::Nonredirects);
    }
    let mut allpages = allpages.generate(&bot);

    let mut interval = tokio::time::interval(Duration::from_millis(args.delay));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Delay);

    while let Some(page) = allpages.recv().await {
        let mut page = page?;

        println!("* {}", page.title());

        if args.edit_refresh {
            page = {
                let wt = page.wikitext().await?;
                let (new_page, resp) =
                    page.save(wt, &SaveOptions::summary(&args.summary)).await?;
                assert!(resp.nochange);
                new_page
            };
        }

        let mut retry = 0;
        loop {
            match bot
                .api()
                .post_with_token::<_, Response>(
                    "csrf",
                    &[("action", "purge"), ("titles", page.title())],
                )
                .await
            {
                Ok(resp) => {
                    if resp.purge[0].purged {
                        purged += 1;
                        break;
                    } else {
                        println!("failed to purge: {}", page.title());
                        interval.tick().await;
                        retry += 1;
                    }
                }
                Err(err) => {
                    println!("{:?}", err);
                    interval.tick().await;
                    retry += 1;
                }
            }
            if retry >= 5 {
                break;
            }
        }
    }

    println!("==== Purged {} pages", purged);

    Ok(())
}
