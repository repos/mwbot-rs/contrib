use anyhow::{bail, Result};
use clap::{ArgAction, Parser};
use mwbot::{
    generators::{self, FilterRedirect, Generator},
    Bot, SaveOptions,
};
use regex::RegexBuilder;
use tokio::time::Duration;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    from: Vec<String>,

    #[arg(short, long)]
    to: Vec<String>,

    #[arg(long)]
    max_count: Option<usize>,

    #[arg(short, long)]
    summary: String,

    #[arg(short, long)]
    include_ns: Vec<String>,

    #[arg(short, long)]
    exclude_ns: Vec<String>,

    #[arg(long, default_value_t)]
    exclude_redirect: bool,

    #[arg(long)]
    tag: Option<String>,

    #[arg(short, long, default_value = "true", action = ArgAction::Set)]
    minor: bool,

    #[arg(short, long, default_value = "true", action = ArgAction::Set)]
    case_insensitive: bool,

    #[arg(short, long, default_value = "5000")]
    delay: u64,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();
    let bot = Bot::from_default_config().await?;

    let replaces = args
        .from
        .iter()
        .map(|s| {
            RegexBuilder::new(s)
                .case_insensitive(args.case_insensitive)
                .multi_line(true)
                .build()
                .unwrap()
        })
        .zip(args.to.iter())
        .collect::<Vec<_>>();
    if args.from.len() != args.to.len() {
        bail!("the count of --from and --to must be the same");
    }

    if !args.include_ns.is_empty() && !args.exclude_ns.is_empty() {
        bail!("include NS and exclude NS could not be used togetjer")
    }
    let ns = if !args.include_ns.is_empty() {
        args.include_ns
            .iter()
            .map(|n| {
                // @TODO: replace with Bot.namespace_id
                bot.title_codec()
                    .namespace_map()
                    .get_id(n.as_str())
                    .unwrap_or_else(|| panic!("NS {} not found", n))
            })
            .collect::<Vec<_>>()
    } else {
        // @TODO: NamespaceMap.all_namespaces
        todo!()
    };

    let mut replaced_places = 0;

    let mut interval = tokio::time::interval(Duration::from_millis(args.delay));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Delay);

    for ns in ns {
        println!(
            "========== {} ==========",
            // @TODO: replace with Bot.namespace_name
            bot.title_codec().namespace_map().get_name(ns).unwrap()
        );

        let mut allpages = generators::AllPages::new()
            .namespace(ns as u32)
            .filter_redirect(if args.exclude_redirect {
                FilterRedirect::Nonredirects
            } else {
                FilterRedirect::All
            })
            .generate(&bot);

        while let Some(page) = allpages.recv().await {
            let page = page?;
            let mut wt = page.wikitext().await?;
            let mut matched = 0;
            for (from, to) in &replaces {
                if from.is_match(&wt) {
                    let matches = from.find_iter(&wt).count();
                    matched += matches;

                    wt = from.replace_all(&wt, to.as_str()).to_string();
                }
            }
            if matched != 0 {
                replaced_places += matched;

                let mut opts = SaveOptions::summary(
                    &args.summary.replace("%LEN%", &format!("{}", matched)),
                )
                .mark_as_minor(args.minor);
                if let Some(tag) = &args.tag {
                    opts = opts.add_tag(tag);
                }
                interval.tick().await;
                let (page, resp) = page.save(wt, &opts).await?;
                println!(
                    "= {} ({}x) => {}",
                    page.title(),
                    matched,
                    resp.newrevid.unwrap_or_default()
                );

                if let Some(max) = &args.max_count {
                    if replaced_places >= *max {
                        bail!("Reached max replace count");
                    }
                }
            }
        }
    }
    Ok(())
}
